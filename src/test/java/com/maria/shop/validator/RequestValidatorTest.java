package com.maria.shop.validator;

import org.junit.Assert;
import org.junit.Test;

public class RequestValidatorTest {

    @Test
    public void testValidateWithRandomByColor() {
        Assert.assertTrue(RequestValidator.validate("bouquet random red 1"));
    }

    @Test
    public void testValidateWithRandomByColorWithoutNumber() {
        Assert.assertTrue(RequestValidator.validate("bouquet random red"));
    }

    @Test
    public void testValidateRandom() {
        Assert.assertTrue(RequestValidator.validate("bouquet random 2"));
    }

    @Test
    public void testValidateRandomWithWordAfter() {
        Assert.assertFalse(RequestValidator.validate("bouquet random sdf"));
    }

    @Test
    public void testValidateRandomWithoutNumber() {
        Assert.assertTrue(RequestValidator.validate("bouquet random"));
    }

    @Test
    public void testValidateRandomWithFlowerName() {
        Assert.assertFalse(RequestValidator.validate("bouquet random rose"));
    }

    @Test
    public void testValidateByName() {
        Assert.assertTrue(RequestValidator.validate("bouquet rose 3"));
    }

    @Test
    public void testValidateByNameWithoutNumber() {
        Assert.assertTrue(RequestValidator.validate("bouquet rose"));
    }

    @Test
    public void testValidateForAllWordsWithoutBouquet() {
        Assert.assertFalse(RequestValidator.validate(" random red 2"));
    }

    @Test
    public void testValidateWithWrongBouquetName() {
        Assert.assertFalse(RequestValidator.validate("bouquets random red 2"));
    }

    @Test
    public void testValidateWithoutRandomButWithColor() {
        Assert.assertFalse(RequestValidator.validate("bouquet red 2"));
    }

    @Test
    public void testValidateWithWrongName() {
        Assert.assertFalse(RequestValidator.validate("bouquet SUN 2"));
    }

    @Test
    public void testValidateWithWrongNumber() {
        Assert.assertFalse(RequestValidator.validate("bouquet rose two"));
    }

    @Test
    public void testValidateWithWrongNumberOfParameters() {
        Assert.assertFalse(RequestValidator.validate("bouquet random red 2 one two three"));
    }

    @Test
    public void testValidateWithWrongNumberOfParameters2() {
        Assert.assertFalse(RequestValidator.validate("bouquet random red 2 one"));
    }

    @Test
    public void testValidateEmpty() {
        Assert.assertFalse(RequestValidator.validate(" "));
    }

    @Test
    public void testValidateNameAndWordAfter() {
        Assert.assertFalse(RequestValidator.validate("bouquet rose 2 asd"));
    }

}