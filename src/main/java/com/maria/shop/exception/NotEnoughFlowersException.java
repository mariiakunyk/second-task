package com.maria.shop.exception;

public class NotEnoughFlowersException extends Exception{
    public NotEnoughFlowersException() {
        super("Not enough flowers in shop");
    }
}
