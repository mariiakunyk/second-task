package com.maria.shop.exception;

public class NotValidRequestException extends Exception{
    public NotValidRequestException() {
        super("Not valid request");
    }
}
