package com.maria.shop.exception;

public class NoSuchFlowerException extends Exception{
    public NoSuchFlowerException() {
        super("Not enough flowers in shop");
    }
}
