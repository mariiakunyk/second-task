package com.maria.shop.shop;


import com.maria.shop.exception.NoSuchFlowerException;
import com.maria.shop.exception.NotEnoughFlowersException;
import com.maria.shop.exception.NotValidRequestException;
import com.maria.shop.model.ColorType;
import com.maria.shop.model.Flower;
import com.maria.shop.model.FlowerType;
import com.maria.shop.validator.RequestValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Shop {
    private Seller seller;
    private List<Flower> flowers = new ArrayList<>();

    public Shop() {
        this.seller = new Seller(this);
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    public List<Flower> sellBouquet(String request) throws NotEnoughFlowersException, NotValidRequestException, NoSuchFlowerException {
        if (!RequestValidator.validate(request)) {
            throw new NotValidRequestException();
        }

        String[] requestParams = request.split(" ");

        if (FlowerType.contains(request)) {
            if (requestParams.length < 3) {
                return seller.askFlowers(FlowerType.byName(requestParams[1]), 1);
            } else {
                int number = Integer.parseInt(requestParams[requestParams.length - 1]);
                return seller.askFlowers(FlowerType.byName(requestParams[1]), number);
            }
        }
        if (request.contains("random")) {
            if (ColorType.contains(request)) {
                if (requestParams.length == 3) {
                    return seller.askFlowersAndSortByPrice(requestParams[2]);
                } else {
                    int number = Integer.parseInt(requestParams[requestParams.length - 1]);
                    return seller.askFlowers(requestParams[2], number);
                }
            } else {
                if (requestParams.length == 2) {
                    return seller.askFlowers(1);
                } else {
                    return seller.askFlowersAndSortByPrice(requestParams[requestParams.length - 1]);
                }
            }

        }
        throw new NoSuchElementException();
    }
}
