package com.maria.shop.shop;


import com.maria.shop.exception.NotEnoughFlowersException;
import com.maria.shop.model.Flower;
import com.maria.shop.model.FlowerType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Seller {

    private Shop shop;

    public Seller(Shop shop) {
        this.shop = shop;
    }

    /**
     * Give us  bouquet of random flowers with constant number of flowers in it.
     *
     * @param number - number of flowers
     * @return list of flowers
     */
    public List<Flower> askFlowers(int number) throws NotEnoughFlowersException {
        if (shop.getFlowers().size() < number) {
            throw new NotEnoughFlowersException();
        }

        List<Flower> newBouquet = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            Random random = new Random();
            int randomIndexOfFlowerInShop = random.nextInt(shop.getFlowers().size());
            Flower flower = shop.getFlowers().get(randomIndexOfFlowerInShop);
            newBouquet.add(flower);
            shop.getFlowers().remove(randomIndexOfFlowerInShop);
        }
        return newBouquet;
    }

    /**
     * Give us  bouquet of random flowers with constant number of flowers in it and flowers with the same color.
     *
     * @param color
     * @param number
     * @return
     */
    public List<Flower> askFlowers(String color, int number) throws NotEnoughFlowersException {
        int count = 0;
        for (int i = 0; i < shop.getFlowers().size(); i++) {
            if (shop.getFlowers().get(i).getColor().equals(color)) {
                count++;
            }
        }
        if (count < number) {
            throw new NotEnoughFlowersException();
        }

        class FlowerDTO {
            Flower flower;
            int positionInShop;

            public FlowerDTO(Flower flower, int positionInShop) {
                this.flower = flower;
                this.positionInShop = positionInShop;
            }
        }

        List<FlowerDTO> flowersByColor = new ArrayList<>(count);
        for (int i = 0; i < shop.getFlowers().size(); i++) {
            if (shop.getFlowers().get(i).getColor().equals(color)) {
                flowersByColor.add(new FlowerDTO(shop.getFlowers().get(i), i));
            }
        }

        List<Flower> newBouquet = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            Random random = new Random();
            int randomIndexOfFlowerInShop = random.nextInt(flowersByColor.size());
            Flower flower = flowersByColor.get(randomIndexOfFlowerInShop).flower;
            newBouquet.add(flower);
            shop.getFlowers().remove(flowersByColor.get(randomIndexOfFlowerInShop).positionInShop);
        }
        return newBouquet;
    }

    /**
     * Give us  bouquet of flowers with constant number of flowers in it and type.
     *
     * @param type
     * @param number
     * @return
     */
    public List<Flower> askFlowers(FlowerType type, int number) throws NotEnoughFlowersException {
        int count = 0;
        for (int i = 0; i < shop.getFlowers().size(); i++) {
            if (shop.getFlowers().get(i).getName().equals(type.getName())) {
                count++;
            }
        }
        if (count < number) {
            throw new NotEnoughFlowersException();
        }

        List<Flower> newBouquet = new ArrayList<>(number);
        for (int i = 0; i < shop.getFlowers().size(); i++) {
            if (shop.getFlowers().get(i).getName().equals(type.getName())) {
                newBouquet.add(shop.getFlowers().get(i));
                shop.getFlowers().remove(i);
            }
        }

        return newBouquet;
    }


    public List<Flower> askFlowersAndSortByPrice(String numberOfFlowers) throws NotEnoughFlowersException {
        int number;
        try {
            number = Integer.parseInt(numberOfFlowers);
        } catch (Exception e) {
            number = 1;
        }
        List<Flower> flowers = askFlowers(number);
        Collections.sort(flowers);
        return flowers;
    }

}
