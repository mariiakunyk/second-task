package com.maria.shop.validator;

import com.maria.shop.model.ColorType;
import com.maria.shop.model.FlowerType;

public class RequestValidator {

    private static final String BOUQUET = "bouquet";
    private static final String RANDOM = "random";

    public static boolean validate(String request) {

        String[] requestParams = request.split(" ");
        int size = requestParams.length;
        if (size < 2 || size > 4) {
            return false;
        }

        if (!BOUQUET.equals(requestParams[0])) {
            return false;
        } else if (!RANDOM.equals(requestParams[1])) {
            if (!FlowerType.isNameValid(requestParams[1])) {
                return false;
            }
        }

        if (BOUQUET.equals(requestParams[0])) {
            if (FlowerType.isNameValid(requestParams[size - 1])){
               if (size < 3){
                   return true;
               }
            }
        }

        if (!((RANDOM.equals(requestParams[size - 1])
                || ColorType.isColorValid(requestParams[size - 1]))) || FlowerType.isNameValid(requestParams[size - 1])) {
            try {
                Integer.parseInt(requestParams[size - 1]);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }
}