package com.maria.shop;

import com.maria.shop.view.ApplicationView;

public class Main {

    public static void main(String[] args){
        ApplicationView app = new ApplicationView();

        //distributor role
        app.populateFlowers();

        //customer role
        app.start();
    }
}
