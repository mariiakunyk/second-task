package com.maria.shop.service;

import com.maria.shop.exception.NoSuchFlowerException;
import com.maria.shop.model.ColorType;
import com.maria.shop.model.Flower;
import com.maria.shop.factory.FlowerFactory;
import com.maria.shop.model.FlowerType;

public class FlowerService {

    public Flower createFlower(String request) throws NoSuchFlowerException {
        String[] informationInArray = request.split(" ");

        FlowerType flowerType = FlowerType.byName(informationInArray[0]);
        Flower flower = FlowerFactory.createFlowerByType(flowerType);
        if (informationInArray.length > 1) {
            if (!ColorType.isColorValid(informationInArray[1])){
                throw new NoSuchFlowerException();
            }
            flower.setColor(informationInArray[1]);
        }
        return flower;
    }

}
