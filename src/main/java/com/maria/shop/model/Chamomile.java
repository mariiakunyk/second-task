package com.maria.shop.model;

public class Chamomile extends Flower {

    public Chamomile() {
        super(FlowerType.CHAMOMILE.getName(),ColorType.WHITE.getColor());
    }

    @Override
    public int getPrice() {
        return 10;
    }

}
