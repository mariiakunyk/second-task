package com.maria.shop.model;

import com.maria.shop.exception.NoSuchFlowerException;

public enum FlowerType {
    ROSE("rose"), TULIP("tulip"), CHAMOMILE("chamomile");

    private String name;

    FlowerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static boolean isNameValid(String flowerName) {
        for (int i = 0; i < values().length; i++) {
            if (values()[i].getName().equals(flowerName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(String request){
        for (FlowerType s : values()) {
            if (request.contains(s.getName())) {
                return true;
            }
        }
        return false;
    }

    public static FlowerType byName(String s) throws NoSuchFlowerException {
        if (s.equals(ROSE.getName())) {
           return  ROSE;
        } else if (s.equals(CHAMOMILE.getName())) {
            return   CHAMOMILE;
        } else if (s.equals(TULIP.getName())) {
            return TULIP;
        } else {
            throw new NoSuchFlowerException();
        }
    }

    public static Flower byNameTakeFlower(String s) throws NoSuchFlowerException {
        if (s.equals(ROSE.getName())) {
            return new Rose();
        } else if (s.equals(CHAMOMILE.getName())) {
            return new Chamomile();
        } else if (s.equals(TULIP.getName())) {
            return new Tulip() ;
        } else {
            throw new NoSuchFlowerException();
        }
    }
}
