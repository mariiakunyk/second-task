package com.maria.shop.model;

public abstract class Flower implements Marketable, Comparable<Flower> {
    private String name;
    private String color;

    public Flower(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return name + " " + color + " " + getPrice();
    }

    public int compareTo(Flower compareFlower) {
        return this.getPrice() > compareFlower.getPrice() ? 1 : -1;
    }
}