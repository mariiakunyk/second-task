package com.maria.shop.model;

public class Rose extends Flower {

    public Rose() {
        super(FlowerType.ROSE.getName(), ColorType.RED.getColor());
    }


    @Override
    public int getPrice() {
        return 30;
    }
}
