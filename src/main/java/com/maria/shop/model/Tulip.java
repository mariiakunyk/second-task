package com.maria.shop.model;

public class Tulip extends Flower  {

    public Tulip() {
        super(FlowerType.TULIP.getName(), ColorType.BLUE.getColor());
    }

    @Override
    public int getPrice() {
        return 20;
    }
}
