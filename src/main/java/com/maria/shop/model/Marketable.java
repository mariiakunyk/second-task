package com.maria.shop.model;

public interface Marketable {
    int getPrice();
}
