package com.maria.shop.model;

public enum ColorType {
    BLUE("blue"), RED("red"), WHITE("white");

    private String color;

    ColorType(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public static boolean contains(String request){
        for (ColorType s : values()) {
            if (request.contains(s.getColor())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isColorValid(String color){
        for (int i = 0; i < values().length; i++) {
            if (values()[i].getColor().equals(color)){
                return true;
            }
        }
        return false;
    }

}
