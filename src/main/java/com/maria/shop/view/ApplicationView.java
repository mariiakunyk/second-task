package com.maria.shop.view;

import com.maria.shop.exception.NoSuchFlowerException;
import com.maria.shop.exception.NotEnoughFlowersException;
import com.maria.shop.exception.NotValidRequestException;
import com.maria.shop.model.Flower;
import com.maria.shop.service.FlowerService;
import com.maria.shop.shop.Shop;

import java.util.List;
import java.util.Scanner;

public class ApplicationView {
    private Shop shop = new Shop();
    private FlowerService flowerService = new FlowerService();

    public void start() {
        String request;
        System.out.println("Hello, do u want something?");
        do {
            System.out.print("> ");
            Scanner sc = new Scanner(System.in);
            request = sc.nextLine();
            try {
                if (!"bye".equals(request)) {
                    List<Flower> flowers = shop.sellBouquet(request);
                    for (Flower flower : flowers) {
                        System.out.println(flower);
                    }
                }
            } catch (NotEnoughFlowersException e) {
                System.out.println("[error] Not enough flowers in my shop");
            } catch (NotValidRequestException e) {
                System.out.println("[error] Not valid request");
            } catch (NoSuchFlowerException e) {
                System.out.println("[error] No such flower");
            }
        } while (!"bye".equals(request));
    }

    public void populateFlowers() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("write information about new model");
            String infoAboutFLower = scanner.nextLine();

            if ("end".equals(infoAboutFLower)) {
                System.out.println("\tEnd of flowers population");
                return;
            }

            try {
                Flower flower = flowerService.createFlower(infoAboutFLower);

                shop.addFlower(flower);
            } catch (NoSuchFlowerException e) {
                System.out.println("It's not possible to create such model");
            }

        }
    }
}
