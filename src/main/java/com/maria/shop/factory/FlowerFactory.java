package com.maria.shop.factory;

import com.maria.shop.exception.NoSuchFlowerException;
import com.maria.shop.model.*;

public class FlowerFactory {
    public static Flower createFlowerByType(FlowerType flowerType) throws NoSuchFlowerException {
        if (FlowerType.ROSE == flowerType) {
            return new Rose();
        } else if (FlowerType.CHAMOMILE == flowerType) {
            return new Chamomile();
        } else if (FlowerType.TULIP == flowerType) {
            return new Tulip();
        } else {
            throw new NoSuchFlowerException();
        }
    }
}
